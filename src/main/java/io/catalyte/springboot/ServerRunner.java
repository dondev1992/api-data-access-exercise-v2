/**
@version - API Data Access Exercise v2
@task - To create a springboot web server with accessible endpoints that allow the user to perform basic CRUD
        functions and some specialize functions using the N-Tier layer architecture.
@author - Don Moore and Richy Phongsavath
*/

package io.catalyte.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerRunner.class);
    }
}
