package io.catalyte.springboot.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    private String make;

    private String model;

    private Integer year;

    @OneToMany(mappedBy = "vehicle", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Review> reviews = new HashSet<>();

    public Vehicle() {
    }

    public Vehicle(String type, String make, String model, Integer year, Set<Review> reviews ) {
        this.type = type;
        this.make = make;
        this.model = model;
        this.year = year;
        this.reviews = reviews;
    }

    public Vehicle(String type, String make, String model, Integer year) {
        this.type = type;
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }
}
