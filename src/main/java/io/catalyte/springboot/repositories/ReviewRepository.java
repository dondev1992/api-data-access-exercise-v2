package io.catalyte.springboot.repositories;

import io.catalyte.springboot.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findByVehicle_MakeAndVehicle_Model(String make, String model);

    Long countByVehicle_MakeAndVehicle_Model(String make, String model);

    void deleteAllByUsername(String username);

    List<Review> findAllByUsername (String username);

    int countByRating (int rating);

}
