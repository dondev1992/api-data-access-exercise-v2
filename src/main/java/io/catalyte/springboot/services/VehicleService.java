package io.catalyte.springboot.services;

import io.catalyte.springboot.entities.Vehicle;
import java.util.List;
import java.util.Optional;

public interface VehicleService {

    List<Vehicle> GetVehicles();

    void AddVehicle(Vehicle vehicle);

    void UpdateVehicle(Vehicle vehicle);

    Optional<Vehicle> GetVehicleById(Long id);

    List<Vehicle> FindAllByMakeAndModel(String make, String model);

    List<Vehicle> FindByTypeOrMakeOrModelOrYear(String type, String make, String model, Integer year);

    void DeleteVehicleById(Long id);

    List<Vehicle>getVehiclesByType(String type);

    List<Vehicle>getVehiclesByMake(String make);

    List<Vehicle>getVehiclesByModel(String model);

    List<Vehicle>getVehiclesByYear(Integer year);

    int getCountOfModel(String model);

}
